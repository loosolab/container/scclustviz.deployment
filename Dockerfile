FROM rocker/shiny:3.6.1
LABEL maintainer="Jens Preussner <jens.preussner@mpi-bn.mpg.de>"

RUN \
    # Install additional tools
    ## git-lfs in current base image requires backports
    echo 'deb http://http.debian.net/debian stretch-backports main' > /etc/apt/sources.list.d/stretch-backports-main.list && \
    ## Assure up-to-date package lists
    apt-get update && \
    ## Assure up-to-date environment
    apt-get dist-upgrade --assume-yes && \
    apt-get install --assume-yes --no-install-recommends \
      ## Fetch additional libraries
      libssh2-1-dev libgit2-dev \
      ## Fetch the additional tools
      git-lfs gnupg ssh-client \
      # Install libraries needed for compilation of the autonomics toolkit
      ## Dependencies of rgl
      libx11-dev libglu1-mesa-dev \
      ## Dependencies of roxygen2
      libxml2-dev zlib1g-dev \
      ## Dependency of RCurl 
      libcurl4-openssl-dev \
      ## Dependencies of multipanelfigure (etc.)
      libmagick++-dev && \
    # Clean the cache(s)
    apt-get clean && \
    rm -r /var/lib/apt/lists/* && \
    # Install R infrastructure
    sudo -u shiny Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/scclustviz.deployment/raw/master/setup.R")' && \
    # Set the time zone
    sh -c "echo 'Europe/Berlin' > /etc/timezone" && \
    dpkg-reconfigure -f noninteractive tzdata && \
    # Clean /tmp (downloaded packages)
    rm -r /tmp/* && \
    # Change ownerships
    chmod -R ugo+wrX /var/log/shiny-server/ && \
    chown -R shiny:shiny /srv/shiny-server/ && \
    # Clean up existing sample apps
    rm -rf /srv/shiny-server/[0-9][0-9]_* /srv/shiny-server/index.html /srv/shiny-server/sample-apps && \

CMD ["Rscript", "/srv/shiny-server/run.R"]
